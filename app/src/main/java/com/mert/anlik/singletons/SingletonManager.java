package com.mert.anlik.singletons;

import android.app.Activity;

import com.mert.anlik.ui.base.BaseActivity;

public class SingletonManager {
    //Activity
    private Activity activity;
    //User
    public SingletonUser singletonUser;

    //Info
    public SingletonInfo singletonInfo;

    public SingletonManager(Activity activity) {
        this.activity = activity;
    }

    public SingletonUser getSingletonUser() {
        return singletonUser.getInstance();
    }

    public SingletonInfo getSingletonInfo() {
        return singletonInfo.getInstance(activity);
    }
}
