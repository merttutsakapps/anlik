package com.mert.anlik.singletons;

import android.app.Activity;
import android.util.Log;

import com.mert.anlik.data.model.User;

public class SingletonUser {
    private static SingletonUser ourInstance;

    public User user;

    private Activity activity;

    private SingletonUser() {
        user = new User();
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void clear() {
        this.user = null;
    }

    public static SingletonUser getInstance() {
        if (ourInstance == null) {
            ourInstance = new SingletonUser();
            Log.d(SingletonRealmHelper.class.getClass().getSimpleName(), "SingletonUser ayağa kaldırıldı.");
        }

        return ourInstance;
    }
}
