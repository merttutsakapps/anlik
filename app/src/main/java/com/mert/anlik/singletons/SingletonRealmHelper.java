package com.mert.anlik.singletons;

import android.app.Activity;
import android.util.Log;

import com.mert.anlik.data.db.realms.RealmHelper;

public class SingletonRealmHelper extends RealmHelper {
    private static SingletonRealmHelper ourInstance;

    private SingletonRealmHelper(Activity activity) {
        super(activity);
    }

    public static SingletonRealmHelper getInstance(Activity activity) {
        if (ourInstance == null) {
            ourInstance = new SingletonRealmHelper(activity);
            Log.d(SingletonRealmHelper.class.getClass().getSimpleName(), "SingletonRealmHelper ayağa kaldırıldı.");
        }

        return ourInstance;
    }
}