package com.mert.anlik.data.db;

import android.app.Activity;

import com.mert.anlik.data.db.firebase.FirebaseHelper;
import com.mert.anlik.data.db.realms.RealmHelper;
import com.mert.anlik.data.db.sharedpreferences.SharedPreferencesHelper;
import com.mert.anlik.singletons.SingletonUser;

public class DataManager {

    //Activity
    private Activity activity;

    //User
    private SingletonUser singletonUser;

    //SharedPreferencesHelper
    private SharedPreferencesHelper sharedPreferencesHelper;

    //RealmHelper
    private RealmHelper realmHelper;

    //FireBase
    private FirebaseHelper firebaseHelper;

    public DataManager(Activity activity) {
        this.activity = activity;
    }

    //Sign
    public void signIn() {
        if (getFirebaseHelper().getUser().getEmail() != null) {
            String mail = getFirebaseHelper().getEmail();
            sharedPreferencesHelper.setEmail(mail);
            singletonUser.user.setEmail(mail);
        }
        if (getFirebaseHelper().getUser().getDisplayName() != null) {
            String name = getFirebaseHelper().getUserName();
            sharedPreferencesHelper.setUserName(name);
            singletonUser.user.setName(name);
        }
        if (getFirebaseHelper().getUser().getUid() != null) {
            String id = getFirebaseHelper().getId();
            sharedPreferencesHelper.setId(id);
            singletonUser.user.setId(id);
        }
        if (getFirebaseHelper().getUser().getPhoneNumber() != null) {
            String phoneNumber = getFirebaseHelper().getPhoneNumber();
            sharedPreferencesHelper.setPhoneNumber(phoneNumber);
            singletonUser.user.setPhoneNumber(phoneNumber);
        }
        if (getFirebaseHelper().getPhotoUrl() != null) {
            String photoUrl = getFirebaseHelper().getPhotoUrl().toString();
            sharedPreferencesHelper.setPhotoUrl(photoUrl);
            singletonUser.user.setPhotoUrl(photoUrl);
        }
    }

    public void signOut() {
        firebaseHelper.signOut();
        clearLocal();
    }

    //Getters
    public SharedPreferencesHelper getSharedPreferencesHelper() {
        if (sharedPreferencesHelper == null) {
            sharedPreferencesHelper = new SharedPreferencesHelper(activity);
        }

        return sharedPreferencesHelper;
    }

    public RealmHelper getRealmHelper() {
        if (realmHelper == null) {
            realmHelper = new RealmHelper(activity);
        }

        return realmHelper;
    }

    public FirebaseHelper getFirebaseHelper() {
        if (firebaseHelper == null) {
            firebaseHelper = new FirebaseHelper(activity);
        }

        return firebaseHelper;
    }

    //Logged
    public void setLogIn() {
        sharedPreferencesHelper.isLogin(true);
    }

    public Boolean getLogInMode() {
        return sharedPreferencesHelper.getLoginMode();
    }

    //Local
    public void clearLocal() {
        sharedPreferencesHelper.clear();
        realmHelper.getRealm().deleteAll();
        singletonUser.clear();
    }

}
