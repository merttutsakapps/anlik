package com.mert.anlik.data.db.sharedpreferences;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.mert.anlik.data.model.User;
import com.mert.anlik.tools.encryption.RSA;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.NoSuchPaddingException;

public class SharedPreferencesHelper {
    public static final String MY_PREFS = "my_prefs";
    public static final String ID = "id";
    public static final String USERNAME = "username";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String EMAIL = "email";
    public static final String PHOTO_URL = "photo_url";
    public static final String LOGIN_MODE = "login_mode";

    private android.content.SharedPreferences preferences;
    private android.content.SharedPreferences.Editor editor;

    //Activity
    private Activity activity;

    public SharedPreferencesHelper(Activity activity) {
        this.activity = activity;

        this.preferences = this.activity.getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE);
        this.editor = this.preferences.edit();
    }

    //Id
    public long getId() {
        long id = preferences.getLong(ID, -1);
        Log.d(this.getClass().getSimpleName(), "setId, -id :" + id);
        return id;
    }

    public boolean setId(long id) {
        Log.d(this.getClass().getSimpleName(), "setId, -id :" + id);
        return editor.putLong(ID, id).commit();
    }

    //User Name
    public String getUserName() {
        String userName = preferences.getString(USERNAME, "");
        Log.d(this.getClass().getSimpleName(), "setUserName, -user name :" + userName);
        return userName;
    }

    public boolean setUserName(String userName) {
        Log.d(this.getClass().getSimpleName(), "setUserName, -user name :" + userName);
        return editor.putString(USERNAME, userName).commit();
    }

    //Name
    public String getName() {
        String name = preferences.getString(NAME, "");
        Log.d(this.getClass().getSimpleName(), "setName, -name :" + name);
        return name;
    }

    public boolean setName(String name) {
        Log.d(this.getClass().getSimpleName(), "setName, -name :" + name);
        return editor.putString(NAME, name).commit();
    }

    //Surname
    public String getSurname() {
        String surname = preferences.getString(SURNAME, "");
        Log.d(this.getClass().getSimpleName(), "setSurName, -surname :" + surname);
        return surname;
    }

    public boolean setSurname(String surname) {
        Log.d(this.getClass().getSimpleName(), "setSurName, -surname :" + surname);
        return editor.putString(SURNAME, surname).commit();
    }

    //Phone Number
    public String getPhoneNumber() {
        String phoneNumber = preferences.getString(PHONE_NUMBER, "");
        Log.d(this.getClass().getSimpleName(), "setPhoneNumber, -surname :" + phoneNumber);
        return phoneNumber;
    }

    public boolean setPhoneNumber(String phoneNumber) {
        Log.d(this.getClass().getSimpleName(), "setPhoneNumber, -surname :" + phoneNumber);
        return editor.putString(PHONE_NUMBER, phoneNumber).commit();
    }

    //E Mail
    public String getEmail() {
        String email = preferences.getString(EMAIL, "");
        Log.d(this.getClass().getSimpleName(), "setEmail, -email :" + email);
        return email;
    }

    public boolean setEmail(String email) {
        Log.d(this.getClass().getSimpleName(), "setEmail, -email :" + email);
        return editor.putString(EMAIL, email).commit();
    }

    //Photo Url
    public String getPhotoUrl() {
        String photoUrl = preferences.getString(PHOTO_URL, "");
        Log.d(this.getClass().getSimpleName(), "setPhotoUrl, -photoUrl :" + photoUrl);
        return photoUrl;
    }

    public boolean setPhotoUrl(String photoUrl) {
        Log.d(this.getClass().getSimpleName(), "setPhotoUrl, -photoUrl :" + photoUrl);
        return editor.putString(PHOTO_URL, photoUrl).commit();
    }

    //LOGIN MODE
    public boolean setLoginMode(boolean loginMode) {
        Log.d(this.getClass().getSimpleName(), "Login Mode =>" + loginMode);
        return editor.putBoolean(LOGIN_MODE, loginMode).commit();
    }

    public boolean getLoginMode() {
        boolean isLogin = preferences.getBoolean(LOGIN_MODE, false);
        Log.d(this.getClass().getSimpleName(), "Login Mode =>" + isLogin);
        return isLogin;
    }

    public boolean clear() {
        Log.d(this.getClass().getSimpleName(), "clear");
        return editor.clear().commit();
    }
}
