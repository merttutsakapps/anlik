package com.mert.anlik.data.db.firebase;

import android.app.Activity;
import android.net.Uri;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseHelper {

    //FireBase
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    //Activity
    private Activity activity;

    public FirebaseHelper(Activity activity) {
        this.activity = activity;

        firebaseAuth = FirebaseAuth.getInstance();
    }

    public boolean isLogin() {
        firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser != null) {
            return true;
        }
        return false;
    }

    public FirebaseUser getUser() {
        return firebaseUser;
    }

    //Id
    public String getId(){
        return firebaseUser.getUid();
    }

    //Email
    public String getEmail(){
        return firebaseUser.getEmail();
    }

    //Phone
    public String getPhoneNumber(){
        return firebaseUser.getPhoneNumber();
    }

    //Display Name
    public String getUserName() {
        return firebaseUser.getDisplayName();
    }

    //Photo Url
    public Uri getPhotoUrl() {
        return firebaseUser.getPhotoUrl();
    }

    //Sign Out
    public void signOut(){
        firebaseAuth.signOut();
    }

}
