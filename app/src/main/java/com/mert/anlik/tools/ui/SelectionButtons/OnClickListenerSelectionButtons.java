package com.mert.anlik.tools.ui.SelectionButtons;


public interface OnClickListenerSelectionButtons {
    void OnClick(int selection);
}
