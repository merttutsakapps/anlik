package com.mert.anlik.ui.activities;

import android.os.Bundle;

import com.mert.anlik.R;
import com.mert.anlik.ui.base.BaseActivity;
import com.mert.anlik.ui.signin.SignInFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private void isLocalSession() {
        if (!dataManager.getLogInMode()) {
            initView(new SignInFragment());
        } else {
            /* ---USER CREATE--- */
            //initView(new ...());
        }
    }

    private void isPublicSession() {
        if (!dataManager.firebase()) {
            initView(new SignInFragment());
        } else {
            //User Create --> LOGIN
            dataManager.setUser();
            //initView(new ...());
        }
    }
}
