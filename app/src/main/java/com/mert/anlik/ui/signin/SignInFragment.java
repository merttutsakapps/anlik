package com.mert.anlik.ui.signin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mert.anlik.R;
import com.mert.anlik.ui.base.BaseFragment;
import com.mert.anlik.ui.signup.SignUpFragment;

import butterknife.BindView;
import butterknife.OnClick;

public class SignInFragment extends BaseFragment {


    @BindView(R.id.editText_username_login)
    private EditText editTextUserName;
    @BindView(R.id.editText_password_login)
    private EditText editTextPassword;
    @BindView(R.id.button_sign_in_login)
    private Button buttonSignIn;
    @BindView(R.id.button_sign_up_login)
    private Button buttonSignUp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        return view;
    }

    @OnClick(R.id.button_sign_in_login)
    public void submitSignIn() {
        Toast.makeText(baseActivity, "HOŞGELDİN " + "!", Toast.LENGTH_SHORT).show();
        //initFragment(new ..());
    }

    @OnClick(R.id.button_sign_up_login)
    public void submitSignUp() {
        //replaceFragment(new SignUpFragment());
    }

}
