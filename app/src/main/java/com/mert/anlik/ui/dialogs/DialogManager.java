package com.mert.anlik.ui.dialogs;

import android.app.Activity;

public class DialogManager {

    private Activity activity;

    //Close Dialog
    private CloseDialog closeDialog;

    //Progress Dialog
    private ProgressDialog progressDialog;

    public DialogManager(Activity activity) {
        this.activity = activity;
    }

    public CloseDialog getCloseDialog() {

        if (closeDialog == null){
            closeDialog = new CloseDialog(activity);
        }

        return closeDialog;
    }

    public ProgressDialog getProgressDialog() {

        if (progressDialog == null){
            progressDialog = new ProgressDialog(activity);
        }

        return progressDialog;
    }
}
